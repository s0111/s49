import React, { useState, useEffect } from 'react';
import CourseCard from './CourseCard';

export default function UserView({coursesData}) {
    
    const [courses, setCourses] = useState([])

    useEffect(() => {

        const coursesArr = coursesData.map(course => {
            //only render the active courses
            if(course.isActive === true){
                return (
                    <CourseCard courseProp = { course } key={course._id}/>
                )
            } else {
                return null;
            }
        })
        setCourses(coursesArr);
    }, [coursesData])
    
    return(
        // <h1> User Dashboard </h1>

        <>
             { courses }
        </>
    )
}