import React, { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';


export default function AdminView(props) {
    

    const { coursesData } = props;
	const [courses, setCourses] = useState([])

    
    useEffect(() => {
		const coursesArr = coursesData.map(course => {
			return (
				<tr key={course._id}>
					<td>{course._id}</td>
					<td>{course.name}</td>
					<td>{course.description}</td>
					<td>{course.price}</td>
					<td className={course.isActive ? "text-success" : "text-danger"}>
						{course.isActive ? "Availabe" : "Unavailabe"}
					</td>	
				</tr>


				)
		})

		setCourses(coursesArr)

	}, [coursesData])

    // end of use effect
    
    return(

        <>
            <div className="text-center my-4">
                <h1> Admin Dashboard </h1>
            </div>

           


            <Table striped bordered hover responsive>
                <thead className="bg-dark text-white">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Availability</th>
                        <th>Actions</th>
                    </tr>
                </thead>

                <tbody>
                    {courses}
                </tbody>
            </Table>
        </>
        
    )
}


